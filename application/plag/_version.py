"""Sets the overall pipeline version used for all pipeline steps being built during CI/CD."""
__version__ = "1.1.1+local"
__version__major__ = __version__.split(".", maxsplit=1)[0]
